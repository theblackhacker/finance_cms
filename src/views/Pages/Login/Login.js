import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import ReactDOM from 'react-dom';
import FacebookLogin from 'react-facebook-login';
import axios from 'axios';
import GoogleLogin from 'react-google-login';
import '../../../scss/login.scss';
import {USER_INFO_URL} from  '../../../server_links/link';


var indents = <div className="column">
<div className="row">
  <p>℧</p>
</div>
<div className="row">
  <p>ℵ</p>
</div>
<div className="row">
  <p>⁁</p>
</div>
<div className="row">
  <p>ℏ</p>
</div>
<div className="row">
  <p>ℌ</p>
</div>
<div className="row">
  <p>ℑ;</p>
</div>
<div className="row">
  <p>ℰ</p>
</div>
<div className="row">
  <p>Ч</p>
</div>
<div className="row">
  <p>∉</p>
</div>
<div className="row">
  <p>∑</p>
</div>
<div className="row">
  <p>≈</p>
</div>
<div className="row">
  <p>⊄</p>
</div>
<div className="row">
  <p>ζ</p>
</div>
<div className="row">
  <p>ξ</p>
</div>
<div className="row">
  <p>ϑ</p>
</div>
<div className="row">
  <p>℧</p>
</div>
<div className="row">
  <p>ℵ</p>
</div>
<div className="row">
  <p>⁁</p>
</div>
<div className="row">
  <p>ℏ</p>
</div>
<div className="row">
  <p>ℌ</p>
</div>
<div className="row">
  <p>ℑ;</p>
</div>
<div className="row">
  <p>ℰ</p>
</div>
<div className="row">
  <p>Ч</p>
</div>
<div className="row">
  <p>∉</p>
</div>
<div className="row">
  <p>∑</p>
</div>
<div className="row">
  <p>≈</p>
</div>
<div className="row">
  <p>⊄</p>
</div>
<div className="row">
  <p>ζ</p>
</div>
<div className="row">
  <p>ξ</p>
</div>
<div className="row">
  <p>ϑ</p>
</div>
<div className="row">
  <p>℧</p>
</div>
<div className="row">
  <p>ℵ</p>
</div>
<div className="row">
  <p>⁁</p>
</div>
<div className="row">
  <p>ℏ</p>
</div>
<div className="row">
  <p>ℌ</p>
</div>
<div className="row">
  <p>ℑ;</p>
</div>
<div className="row">
  <p>ℰ</p>
</div>
<div className="row">
  <p>Ч</p>
</div>
<div className="row">
  <p>∉</p>
</div>
<div className="row">
  <p>∑</p>
</div>
<div className="row">
  <p>≈</p>
</div>
<div className="row">
  <p>⊄</p>
</div>
<div className="row">
  <p>ζ</p>
</div>
<div className="row">
  <p>ξ</p>
</div>
<div className="row">
  <p>ϑ</p>
</div>
</div>;
var indents_sum = [];
for (var i=0;i<16;i++){
  indents_sum.push(indents);
}

  
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { isAuthenticated: false, user: null, token: '', allow_redirect: false};
        this.responseFacebook = this.responseFacebook.bind(this);
    }
    logout = () => {
      this.setState({isAuthenticated: false, token: '', user: null})
    };
    facebookResponse = (e) => {};

    googleResponse = (e) => {};


    sign_in(token,type){
      console.log("token ,type",token,type);
      axios.get(USER_INFO_URL,
      {
        headers: {
          Authorization:token
        }
      }).then(response => {
          console.log('data là: ', response.data)
          if (response.data.data.role === 1){
           window.location.href = '/#/dashboard';
          }          
      })
    }
  
    responseFacebook(response) {
        if (response){
            console.log(response);
            this.state.token = response.accessToken;
            console.log("DCM",this.state.token);
            this.setCookie('current_user',this.state.token, 1);
            this.setCookie('type','facebook', 1);
            this.props.updateCurrentUser(response);
            // window.location.href = '/'; 
            this.sign_in(response.accessToken, 'facebook')
        }
      }
    componentClicked = () =>  this.setState({allow_redirect: true});


    responseGoogle = (response) => {
        console.log(response);
        this.state.token = response.accessToken;
        console.log("DCM",this.state.token);
        this.setCookie('current_user',this.state.token, 1);
        this.setCookie('type','google', 1);
        this.props.updateCurrentUser(response);
        this.sign_in(response.accessToken, 'google');
      }

    //  Hàm get cookie
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

       
    }

    //Hàm lấy cookie
    getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  //Second
  componentWillMount(){

    //Cách set cookie
    // this.setCookie('token', 'Facebook token', 1)

    //Cách lấy cookie
    // this.getCookie('token')

    //Cách chuyển trang
    // window.location.href = "url mới"

    // axios.get('https://example.com/getSomething', {
    //   headers: {
    //     Authorization: getCookie()
    //   }
    // }).then(response => {
    //   console.log(response)
    // })
    var token =   this.getCookie('current_user');
    // console.log('abc',token);
    // axios.post('http://finance.tinychiu.com/api/register', {
    //     token: this.getCookie('current_user'),
    //     type: "facebook"
    // },
    // {
    //   headers: {
    //     "Access-Control-Allow-Origin": "*"
    //     // Authorization: getCookie()
    //   }
    // }).then(response => {
    //     console.log(response)
    // })
  }

 
  render() {
    return (
      <div className = "login_parent">
        
       
      {indents_sum}
    
      <div className="container">
        <div className="container_inner">
          <div className="container_inner__login">
            <div className="login">
              <div className="login_profile">
                <img className="logo" src= 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/ecotech.svg'/>
                <img className="pulse" src = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/rings.svg'/></div>
              <div className="login_desc">
                <h3>
                  Welcome to CMS
                  <span>Manager</span>
                </h3>
              </div>
              <div className="login_inputs">
                <div className="login_check">
                <div className = "login_facebook">
                  <FacebookLogin
                    appId="451766898686130"
                    autoLoad={false}
                    fields="name,email,picture"
                    onClick = {this.componentClicked}
                    callback={this.responseFacebook}
                    />
                </div>
                    <div className = "login_google">
                    <GoogleLogin
                      clientId="396797287252-k6e0bbd9pdsmbmkqfefsc19sao0h460r.apps.googleusercontent.com"
                      
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogle}
                    />,
                    </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      </div>
    );
    }
}
export default Login;
