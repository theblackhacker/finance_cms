import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import axios from 'axios'
import {ALL_USER_URL,DETAIL_COMPANY_URL,ADD_USER_TO_COMPANY_URL,REMOVE_USER_TO_COMPANY_URL} from  '../../server_links/link';
import Select from 'react-select'



class AddUserToCompany extends Component {

  //First
  constructor(props){
    super(props)

    this.state = {
      listUser: [],
      loading :false,
      current_Company:null,
      selectedOption: null
    }
    this.UserRow = this.UserRow.bind(this);
  }
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  }

   UserRow(props) {
    const user = props.user
    const userLink = `/users/${user.id}`
    const check_have_company = props.nocompany
    const getBadge = (status) => {
      return status === 1 ? 'Đã vào công ty' : 'Chưa vào công ty'
        // status == '0' ? 'Chưa vào công ty' :
        //   status == '' ? 'Chưa vào công ty' :
        //     status === 'Banned' ? 'danger' :
        //       'primary'
    }
    const getBadge_Color = (status) => {
      return status === 1 ? 'Active' : 'danger'
        // status == '0' ? 'Chưa vào công ty' :
        //   status == '' ? 'Chưa vào công ty' :
        //     status === 'Banned' ? 'danger' :
        //       'primary'
    }
    return (
      <tr key={user.id.toString()}>
        <th scope="row"><Link to={userLink}>{user.id}</Link></th>
        <td><Link to={userLink}>{user.fullname}</Link></td>
        <td>{user.created_at}</td>
        <td>{user.role}</td>
        <td><Link to={userLink}><Badge color={getBadge_Color(user.status)}>{getBadge(user.status)}</Badge></Link></td>
        { check_have_company == 'no'?
          (<td><Button  onClick = {this.Add_to_this_company(user.id)} outline color="primary">Thêm</Button></td>):(<td><Button  onClick = {this.Remove_this_company(user.id)} outline color="danger">Xóa</Button></td>)      
        }
      </tr>
    )
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  Remove_this_company = (user_id) => (e) => {
    e.preventDefault()  
    var company_id =  this.props.match.params.id;
    var id = user_id
    var r = window.confirm("Bạn có chắc là muốn xóa thành viên không");
    if (r == true) {
      axios.post(REMOVE_USER_TO_COMPANY_URL, {
        "companyId": company_id,
        "userId": id 
      },
      {
        headers: {
          Authorization: this.getCookie('current_user'),
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status == 200){
          alert(" Đã xóa thành công");
          window.location.reload();
        }
        else {
          alert(" Xảy ra lỗi khi xóa thành viên");
        }
      })
    }
    else{
        alert("Bạn đã chọn Cancel!");
    }
  }


  Add_to_this_company = (user_id) => (e) => {
    e.preventDefault()  
    var company_id =  this.props.match.params.id;
    var id = user_id
    var r = window.confirm("Bạn có chắc là muốn thêm thành viên không");
    if (r == true) {
      axios.post(ADD_USER_TO_COMPANY_URL, {
        "companyId": company_id,
        "userId": id 
      },
      {
        headers: {
          Authorization: this.getCookie('current_user'),
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.status == 200){
          alert(" Đã thêm thành công");
          window.location.reload();
        }
        else {
          alert(" Xảy ra lỗi khi thêm thành viên");
        }
      })
    }
    else{
        alert("Bạn đã chọn Cancel!");
    }
  }
  // //Second
  componentWillMount(){
    //Cách lấy cookie
    var cookie_user = this.getCookie('current_user')
    console.log('cookies', cookie_user);
    // var url_user_no_company = ALL_USER_URL+'?status=0'
    //Cách chuyển trang
    // window.location.href = "url mới"
    console.log("id",this.props);
    var id_company = this.props.match.params.id;
    axios.get(DETAIL_COMPANY_URL + id_company, {
      headers: {
        Authorization: cookie_user,
        'Content-Type': 'application/json'
      }
    }).then(response => {
      // console.log(response)
      if (response.status != 200){
        window.location.href = "/404";
      }
      else {
        console.log('ngao',response)
        this.setState({current_Company:response});
        axios.get(ALL_USER_URL, {
          headers: {
            Authorization: cookie_user,
            'Content-Type': 'application/json'
          }
        }).then(response => {
          console.log(response)
          this.setState({listUser:response, loading:true});
        })
      }
    })




    

    // axios.post('https://example.com/postSomething', {
    //   email: varEmail, //This is data
    //   password: varPassword
    // },
    // {
    //   headers: {
    //     Authorization: getCookie()
    //   }
    // })
  }

  render() {
    var user_no_company = [];
    var user_company = [];
    var current_company = '';
    var userList = [];
    let user_all;
    if (this.state.loading == true){
     current_company = this.state.current_Company.data.data.company
     userList = this.state.listUser.data.data
     user_no_company = userList.filter(function(x) {return x.status != 1});
     user_company = this.state.current_Company.data.data.members;
      console.log('data là:',user_no_company);
      const { selectedOption } = this.state;
      const options = user_no_company.map(function(x){
        return {value:x.fullname, label:x.fullname}
      })
      user_all =  <Row><Col xl={6}>
      <Card>
        <CardHeader>
          <i className="fa fa-align-justify"></i> Thành viên chưa vào công ty <small className="text-muted">({user_no_company.length})</small>
        </CardHeader>
        <CardBody>
          <Table responsive hover>
            <thead>
              <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">registered</th>
                <th scope="col">role</th>
                <th scope="col">status</th>
                <th scope="col">Thêm vào công ty</th>
              </tr>
            </thead>
            <tbody>
              {user_no_company.map((user, index) =>
                <this.UserRow key={index} user={user} nocompany={'no'}/>
               
              )}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </Col>
    <Col xl={6}>
      <Card>
        <CardHeader>
          <i className="fa fa-align-justify"></i> Thành viên đã vào công ty <small className="text-muted">({user_company.length})</small>
        </CardHeader>
        <CardBody>
          <Table responsive hover>
            <thead>
              <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">registered</th>
                <th scope="col">role</th>
                <th scope="col">status</th>
                <th scope="col">Remove</th>
              </tr>
            </thead>
            <tbody>
              {user_company.map((user, index) =>
                <this.UserRow key={index} user={user} nocompany={'yes'}/>
              )}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </Col></Row>;

      if (this.state.selectedOption!= null){
        var name_search = this.state.selectedOption.value
        var user_searched = user_no_company.filter(function(x) {return x.fullname == name_search });
        user_all = <Row><Col xl={12}>
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i> Thành viên <small className="text-muted">({user_searched.length})</small>
          </CardHeader>
          <CardBody>
            <Table responsive hover>
              <thead>
                <tr>
                  <th scope="col">id</th>
                  <th scope="col">name</th>
                  <th scope="col">registered</th>
                  <th scope="col">role</th>
                  <th scope="col">status</th>
                  <th scope="col">Thêm vào công ty</th>
                </tr>
              </thead>
              <tbody>
                {user_searched.map((user, index) =>
                  <this.UserRow key={index} user={user}  nocompany={'no'}/>
                )}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col></Row>
      }
    // const userList = usersData.filter((user) => user.id < 10)
  
      
    return (
      
      <div className="animated fadeIn">
      <Row><Col xl={12}>
        <h1>Công ty{current_company.name}</h1>
      
      
      </Col></Row>
      <Row>
      <Col xl={6} style={{  marginBottom: 20 + 'px' }}>
      <Select
        value={selectedOption}
        onChange={this.handleChange}
        options={options}
      />
      </Col>
      </Row>
        
         {user_all}
        
      </div>
    )
  }
  else{
    return <h1>loading ..</h1>
  }
}
}

export default AddUserToCompany;
