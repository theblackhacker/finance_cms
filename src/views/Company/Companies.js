import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import {Button,  Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Button, Card, CardBody, CardFooter, CardHeader, Col, Table, Container, Badge, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import Modal from 'react-modal';
import axios from 'axios'
import { ALL_COMPANY_URL, DELETE_COMPANY_URL,COMPANY_EDIT_URL } from '../../server_links/link';
import '../../scss/pagination.scss';
import Pagination from "react-js-pagination";


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};



class Companies extends Component {

  //First
  constructor(props) {
    super(props)

    this.state = {
      listUser: [],
      activePage: 1,
      loading: false,
      modalIsOpen: false,
      current_modal: '',
      companyname: '',
      description: '',
      balance: ''

    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.hamXoa = this.hamXoa.bind(this);
    this.getCookie = this.getCookie.bind(this);
    this.UserRow = this.UserRow.bind(this);
    this.Modal = this.Modal.bind(this);
    this.updateInput = this.updateInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }

  openModal = (data) => (e) => {
    e.preventDefault();
    this.setState({ modalIsOpen: true });
    this.setState({ current_modal: data });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }


  //Hàm get cookie
  // setCookie(cname, cvalue, exdays) {
  //   var d = new Date();
  //   d.setTime(d.getTime() + (exdays*24*60*60*1000));
  //   var expires = "expires="+ d.toUTCString();
  //   document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  // }


  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }


  hamXoa = (id) => (e) => {
    e.preventDefault()
    // if(window.confirm("Bấm vào nút OK để tiếp tục") == true){
    //   console.log('lampd');
    // }else{
    console.log('id', id);

    var r = window.confirm("Bạn có chắc là muốn xóa không");
    if (r == true) {
      var user = this.getCookie('current_user');
      console.log("OAOHHOAHOA", user);
      axios.post(DELETE_COMPANY_URL, {
        "id": parseInt(id)
      },
        {
          headers: {
            Authorization: user
          }
        }).then(response => {
          if (response.status == 200) {
            alert(" Đã xóa thành công");
            window.location.reload();
          }
          else {
            alert(" Xảy ra lỗi khi xóa công ty");
          }
          // this.setState({listUser:response, loading:true});
        })


    } else {
      alert("Bạn đã chọn Cancel!");
    }
  }
  Modal(props) {
    // this.setState({modalIsOpen: true});

    var company = this.state.current_modal;
    console.log('data', company);
    return (
      <Modal
        isOpen={this.state.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >

        <h2 ref={subtitle => this.subtitle = subtitle} style={{ float: 'left' }}>{company.name} </h2>
        <button onClick={this.closeModal} style={{ float: 'right' }}>X</button>
        {/* <div></div> */}
        <Form>
          {/* <p className="text-muted">Công ty</p> */}
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-user"></i>
              </InputGroupText>
            </InputGroupAddon>
            <Input onChange={this.updateInput} defaultValue={company.name} type="text" placeholder="tên công ty" autoComplete="username" name="company" />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>@</InputGroupText>
            </InputGroupAddon>
            <Input type="text" defaultValue={company.description} onChange={this.updateInput} placeholder="Mô tả" autoComplete="description" name="description" />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-lock"></i>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="text" pattern="[0-9]*" defaultValue={company.balance} onChange={this.updateInput} placeholder="Số dư" autoComplete="balance" name="balance" />
          </InputGroup>

          <Button onClick={this.handleSubmit} color="success" block>Sửa thông tin</Button>
        </Form>
      </Modal>
    )
  }
  updateInput(event) {
    if (event.target.name == 'company') {
      this.setState({ companyname: event.target.value });
     
    }
    if (event.target.name == 'description') {
      this.setState({ description: event.target.value });
    }
    if (event.target.name == 'balance') {
      this.setState({ balance: event.target.value });
    }
    console.log('state',this.state);
  }


  handleSubmit(){

    if (isNaN(this.state.balance)){
      alert("Số dư phải là số, Vui lòng nhập lại!");
     
    }
    else {
      axios.post(COMPANY_EDIT_URL, {
        "id" : this.state.current_modal.id,
        "name": this.state.companyname, //This is data
        "description": this.state.description,
        "balance": parseInt(this.state.balance)
        },
        {
        headers: {
            'Content-Type':'application/json',
            Authorization: this.getCookie('current_user')
        }
    }).then(response => {
        if (response.status == 200){
            alert("Bạn đã sửa thành công ")
            window.location.reload();
        }
        else{
            alert("Xảy ra lỗi ");
            window.location.href = "/404"
        }
      })
    }

  }


  UserRow(props) {
    const company = props.company
    const companyLink = `/companies/${company.id}`

    const getBadge = (status) => {
      return status === 1 ? 'Đã vào công ty' : 'Chưa vào công ty'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
    }
    const getBadge_Color = (status) => {
      return status === 1 ? 'Active' : 'danger'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
    }
    return (
      <tr key={company.id.toString()}>
        <th scope="row"><Link to={companyLink}>{company.id}</Link></th>
        <td><Link to={companyLink}>{company.name}</Link></td>
        <td>{company.description}</td>
        <td>{company.create_at}</td>
        <td><Link to={companyLink}><Badge color={getBadge_Color(company.status)}>{company.balance}</Badge></Link></td>
        <td><Button onClick={this.openModal(company)} outline color="primary">Sửa</Button></td>
        <td><Button onClick={this.hamXoa(company.id)} outline color="danger">Xóa</Button></td>

      </tr>
    )
  }

  componentWillMount() {
    //Cách lấy cookie
    var cookie_user = this.getCookie('current_user')
    console.log('cookies', cookie_user);
    //Cách chuyển trang
    // window.location.href = "url mới"

    axios.get(ALL_COMPANY_URL, {
      headers: {
        Authorization: cookie_user,
        'Content-Type': 'application/json'
      }
    }).then(response => {
      console.log(response)
      this.setState({ listUser: response, loading: true });
    })
  }

  render() {

    if (this.state.loading == true) {
      var all_companies = this.state.listUser.data.data.data;
      var total_company = all_companies.length;
      var page = this.state.activePage;
      var companies = all_companies.slice(page - 1, 10 * page);
      // const userList = usersData.filter((user) => user.id < 10)

      return (
        <div>
          <div className="animated fadeIn">
            <Row>
              <Col xl={12}>
                <Card>
                  <CardHeader>
                    <i className="fa fa-align-justify"></i> Danh sách công ty <small className="text-muted">({total_company})</small>
                  </CardHeader>
                  <CardBody>
                    <Table responsive hover>
                      <thead>
                        <tr>
                          <th scope="col">id</th>
                          <th scope="col">Tên</th>
                          <th scope="col">Mô tả</th>
                          <th scope="col">Thời gian</th>
                          <th scope="col">Số dư</th>
                          <th scope="col">Sửa thông tin</th>
                          <th scope="col">Xóa</th>
                        </tr>
                      </thead>
                      <tbody>
                        {companies.map((company, index) =>
                          <this.UserRow key={index} company={company} />

                        )}
                      </tbody>
                    </Table>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <this.Modal />
          </div>
          <div className="pagination-all"><Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={total_company}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
          </div>
        </div>
      )
    }
    else {
      return (<h2>Đang tải dữ liệu...</h2>)
    }
  }
}

export default Companies;
