import React, { Component } from 'react';
import { Badge, Card,Button, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import {DETAIL_COMPANY_URL} from  '../../server_links/link';
import axios from 'axios'

import { Link } from 'react-router-dom';


function UserRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 1 ? 'Đã vào công ty' : 'Chưa vào công ty'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
  }
  const getBadge_Color = (status) => {
    return status === 1 ? 'Active' : 'danger'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
  }
  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{user.fullname}</Link></td>
      <td>{user.created_at}</td>
      <td>{user.role}</td>
      <td><Link to={userLink}><Badge color={getBadge_Color(user.status)}>{getBadge(user.status)}</Badge></Link></td>
    </tr>
  )
}


class Company extends Component {
    //First
  constructor(props){
    super(props)

    this.state = {
      Company: [],
      loading :false
    }

  }
  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  // //Second
  componentWillMount(){
    //Cách lấy cookie
    var cookie_user = this.getCookie('current_user')
    console.log('cookies', cookie_user);
    var id_user = this.props.match.params.id
    console.log('id', id_user);
    //Cách chuyển trang
    // window.location.href = "url mới"

    axios.get(DETAIL_COMPANY_URL + id_user, {
      headers: {
        Authorization: cookie_user,
        'Content-Type': 'application/json'
      }
    }).then(response => {
      // console.log(response)
      if (response.status != 200){
        window.location.href = "/404";
      }
      else {
        console.log('ngao',response)
        this.setState({Company:response, loading:true});
      }
    })

    // axios.post('https://example.com/postSomething', {
    //   email: varEmail, //This is data
    //   password: varPassword
    // },
    // {
    //   headers: {
    //     Authorization: getCookie()
    //   }
    // })
  }
  
  render() {
    var user  = ''
    if (this.state.loading == true){
      var company = this.state.Company.data.data.company
      var users_of_company = this.state.Company.data.data.members
      const userDetails = company ? Object.entries(company) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]
      var url_add_user = '/add_user_to_company/' + company.id
      return (
        <div className="animated fadeIn">
        <Link to={url_add_user}>
          <Button outline color="danger" style={{marginBottom:"20px", width:"200px"}}>Thêm User</Button>
          </Link>
          <Row>
            <Col lg={12}>
              <Card>
                <CardHeader>
                  <strong><i className="icon-info pr-1"></i>Company Id: {this.props.match.params.id}</strong>
                </CardHeader>
                <CardBody>
                    <Table responsive striped hover>
                      <tbody>
                        {
                          userDetails.map(([key, value]) => {
                            return (
                              <tr key={key}>
                                <td>{`${key}:`}</td>
                                <td><strong>{value}</strong></td>                     
                              
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Danh sách thành viên <small className="text-muted">({users_of_company.length})</small>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">id</th>
                        <th scope="col">name</th>
                        <th scope="col">registered</th>
                        <th scope="col">role</th>
                        <th scope="col">status</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users_of_company.map((user, index) =>
                        <UserRow key={index} user={user}/>
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
    else{
      return null
    }
  }
}

export default Company;
