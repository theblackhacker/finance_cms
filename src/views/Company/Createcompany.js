import React, { Component } from 'react';
import {DETAIL_COMPANY_URL} from  '../../server_links/link';
import axios from 'axios'
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {CREATE_COMPANY_URL} from  '../../server_links/link';

class Createcompany extends Component {
    constructor(props){
        super(props);
        
        this.state = {
          companyname : '',
          description: '',
          balance: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateInput = this.updateInput.bind(this);
    }
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }
    updateInput(event ){
         if (event.target.name == 'company'){
            this.setState({companyname : event.target.value});
        }
        if (event.target.name == 'description'){
            this.setState({description : event.target.value});
        }
        if (event.target.name == 'balance')  {
            this.setState({balance : event.target.value});
        }
    }
        
    handleSubmit = (event) => {
      event.preventDefault();
      console.log("COOKIES",this.getCookie('current_user') )
        axios.post(CREATE_COMPANY_URL, {
            "name": this.state.companyname, //This is data
            "description": this.state.description,
            "balance": parseInt(this.state.balance)
            },
            {
            headers: {
                'Content-Type':'application/json',
                Authorization: this.getCookie('current_user')
            }
        }).then(response => {
            if (response.status == 200){
                alert("Bạn đã thêm mới thành công ")
            }
            else{
                alert("Xảy ra lỗi ");
                window.location.href = "/404"
            }
          })
    }
  
    render() {
      return (
        <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="8">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form>
                    <h1>Thêm công ty</h1>
                    {/* <p className="text-muted">Công ty</p> */}
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={this.updateInput} type="text" placeholder="tên công ty" autoComplete="username" name = "company" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text"   onChange={this.updateInput} placeholder="Mô tả" autoComplete="description" name = "description" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text"  onChange={this.updateInput}  placeholder="Số dư" autoComplete="balance"  name = "balance"/>
                    </InputGroup>
                   
                    <Button onClick = {this.handleSubmit} color="success" block>Create Company</Button>
                  </Form>
                </CardBody>
               
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      );
    }
  }

  
export default Createcompany;