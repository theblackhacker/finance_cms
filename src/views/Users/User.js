import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import {USER_ID_URL} from  '../../server_links/link';
import axios from 'axios';

class User extends Component {
    //First
  constructor(props){
    super(props)

    this.state = {
      User: [],
      loading :false
    }
    this.Update_Admin_Role = this.Update_Admin_Role.bind(this);

  }
  Update_Admin_Role = (id) => (e) => {
    e.preventDefault()    
    // if(window.confirm("Bấm vào nút OK để tiếp tục") == true){
    //   console.log('lampd');
    // }else{
    //   console.log('lampd + amckam');
    // }
    var r = window.confirm("Bạn có chắc là muốn update quyền Admin cho User này?");
    if (r == true) {
      var user = this.getCookie('current_user');
      console.log("OAOHHOAHOA", id);
      axios.post('https://example.com/postSomething', {
        // email: varEmail, //This is data
        // password: varPassword
      },
      {
        headers: {
          Authorization: user
        }
      }).then(response => {
        if (response.status == 200){
          alert(" Đã xóa thành công");
        }
        else {
          alert(" Xảy ra lỗi khi xóa công ty");
        }
        // this.setState({listUser:response, loading:true});
      })
  
  
    } else {
      alert("Bạn đã hủy thao tác này!");
    }
  }


  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  // //Second
  componentWillMount(){
    //Cách lấy cookie
    var cookie_user = this.getCookie('current_user')
    console.log('cookies', cookie_user);
    var id_user = this.props.match.params.id
    console.log('id', id_user);
    //Cách chuyển trang
    // window.location.href = "url mới"

    axios.get(USER_ID_URL + id_user, {
      headers: {
        Authorization: cookie_user,
        'Content-Type': 'application/json'
      }
    }).then(response => {
      console.log(response)
      this.setState({User:response, loading:true});
    })

    // axios.post('https://example.com/postSomething', {
    //   email: varEmail, //This is data
    //   password: varPassword
    // },
    // {
    //   headers: {
    //     Authorization: getCookie()
    //   }
    // })
  }
  
  render() {
    var user  = ''
    if (this.state.loading == true){
     user = this.state.User.data.data
    
    const userDetails = user ? Object.entries(user) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]

    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong><i className="icon-info pr-1"></i>User id: {this.props.match.params.id}</strong>
              </CardHeader>
              <CardBody>
                  <Table responsive striped hover>
                    <tbody>
                      {
                        userDetails.map(([key, value]) => {
                          if(key =='status' ){
                            if (value == 1){
                            value = 'Đã tham gia công ty'
                            }
                            else{
                              value = 'Chưa tham gia công ty'
                            }
                          }
                          if(key =='role' ){
                            if (value == 1){
                              value = 'Admin'
                              }
                              else{
                                value = 'User thường'
                              }
                          }
                          return (
                            <tr key={key}>
                              <td>{`${key}:`}</td>
                             { key == 'avatar'?(
                               <td>
                               <img src = {value} style={{"width" : "100px"}}></img></td>
                             ):(<td><strong>{value}</strong></td>)                           
                             }
                            </tr>
                          )
                        })
                      }
                      <tr><th colSpan="2"><Button onClick = {this.Update_Admin_Role(this.props.match.params.id)} outline color="primary">Cấp quyền Admin</Button></th></tr>
                    </tbody>
                  </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
    }
    else{
      return null
    }
  }
}

export default User;
