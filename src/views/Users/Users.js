import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import Pagination from "react-js-pagination";
import axios from 'axios'
import {ALL_USER_URL} from  '../../server_links/link';
import '../../scss/pagination.scss'
function UserRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 1 ? 'Đã vào công ty' : 'Chưa vào công ty'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
  }
  const getBadge_Color = (status) => {
    return status === 1 ? 'Active' : 'danger'
      // status == '0' ? 'Chưa vào công ty' :
      //   status == '' ? 'Chưa vào công ty' :
      //     status === 'Banned' ? 'danger' :
      //       'primary'
  }
  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{user.fullname}</Link></td>
      <td>{user.created_at}</td>
      <td>{user.role==1?('Admin'):('User thường')}</td>
      <td><Link to={userLink}><Badge color={getBadge_Color(user.status)}>{getBadge(user.status)}</Badge></Link></td>
    </tr>
  )
}

class Users extends Component {

  //First
  constructor(props){
    super(props)

    this.state = {
      listUser: [],
      activePage:1,
      loading :false
    }
    this.handlePageChange = this.handlePageChange.bind(this)
  }

  //Hàm get cookie
  // setCookie(cname, cvalue, exdays) {
  //   var d = new Date();
  //   d.setTime(d.getTime() + (exdays*24*60*60*1000));
  //   var expires = "expires="+ d.toUTCString();
  //   document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  // }

  //Hàm lấy cookie
  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }
  // //Second
  componentWillMount(){
    //Cách lấy cookie
    var cookie_user = this.getCookie('current_user')
    console.log('cookies', cookie_user);
    //Cách chuyển trang
    // window.location.href = "url mới"

    axios.get(ALL_USER_URL, {
      headers: {
        Authorization: cookie_user,
        'Content-Type': 'application/json'
      }
    }).then(response => {
      console.log(response)
      this.setState({listUser:response, loading:true});
    })

    // axios.post('https://example.com/postSomething', {
    //   email: varEmail, //This is data
    //   password: varPassword
    // },
    // {
    //   headers: {
    //     Authorization: getCookie()
    //   }
    // })
  }

  render() {
    var userList = [];
    if (this.state.loading == true){
     var userAll = this.state.listUser.data.data;
     var page  = this.state.activePage;
     userList = userAll.slice(page-1,10*page);
      console.log('data là:',userList);
    var total_user = userAll.length;
    // const userList = usersData.filter((user) => user.id < 10)

    return (
      <div>
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Danh sách thành viên <small className="text-muted">({userList.length})</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">Tên</th>
                      <th scope="col">Ngày đăng ký</th>
                      <th scope="col">Vai trò</th>
                      <th scope="col">Trạng thái</th>
                    </tr>
                  </thead>
                  <tbody>
                    {userList.map((user, index) =>
                      <UserRow key={index} user={user}/>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
        
      </div>
      <div className="pagination-all"><Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={10}
          totalItemsCount={total_user}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange}
        /></div>
      </div>
    )
    }
    else{
      return(<h2>Đang tải dữ liệu</h2>)
    }
  }
}

export default Users;
