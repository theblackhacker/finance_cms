const SERVER_URI = "http://finance.tinychiu.com"

//User
// export const REGISTER_URL = SERVER_URI+"/api/register"
export const ALL_USER_URL = SERVER_URI + "/api/users/list"
export const USER_ID_URL = SERVER_URI + "/api/user/"
export const USER_INFO_URL = SERVER_URI + "/api/user"
//company
export const ALL_COMPANY_URL = SERVER_URI + "/api/companies/list"
export const DETAIL_COMPANY_URL = SERVER_URI + "/api/company/"
export const CREATE_COMPANY_URL = SERVER_URI + "/api/company/create"
export const DELETE_COMPANY_URL = SERVER_URI + "/api/company/delete"
export const ADD_USER_TO_COMPANY_URL = SERVER_URI + "/api/user/addCompany"
export const REMOVE_USER_TO_COMPANY_URL = SERVER_URI + "/api/user/deleteCompany"
export const COMPANY_EDIT_URL = SERVER_URI + "/api/company/edit"