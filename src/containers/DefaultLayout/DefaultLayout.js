import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import axios from 'axios';
import {USER_INFO_URL} from  '../../server_links/link';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

function catchError( error ){

  console.log( error );
  window.location.href= '/#/login';

}
class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { isAuthenticated: false, current_user: null, token: '', loading: false};
    
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  signOut(e) {
    e.preventDefault()
    this.setCookie('current_user','',1) ;
    this.setCookie('type','',1) ;
    this.props.history.push('/login');
  }
  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  componentWillMount(){
    var token =   this.getCookie('current_user');
    console.log("token",token);

    var type =   this.getCookie('type');
    if (token && token!='undefined'){
      console.log("type",type);
      axios.get(USER_INFO_URL, 
      {
        headers: {
          Authorization:token
        }
      }).then(response => {
          console.log('data là: ', response.data)
          if (response.data.data.role === 1){
          this.setState({isAuthenticated:true, current_user: response.data.data,token:token});

          }          
      }).catch(catchError); 
      
    }
    else{
      this.setCookie('current_user','',1) ;
      this.setCookie('type','',1) ;
      window.location.href= '/#/login'
    }
  }

  render() {
    if (this.state.isAuthenticated == true){
      return (
        <div className="app">
          <AppHeader fixed>
            <Suspense  fallback={this.loading()}>
              <DefaultHeader currentUser={this.state.current_user} onLogout={e=>this.signOut(e)}/>
            </Suspense>
          </AppHeader>
          <div className="app-body">
            <AppSidebar fixed display="lg">
              <AppSidebarHeader />
              <AppSidebarForm />
              <Suspense>
              <AppSidebarNav navConfig={navigation} {...this.props} />
              </Suspense>
              <AppSidebarFooter />
              <AppSidebarMinimizer />
            </AppSidebar>
            <main className="main">
              <AppBreadcrumb appRoutes={routes}/>
              <Container fluid>
                <Suspense fallback={this.loading()}>
                  <Switch>
                    {routes.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={props => (
                            <route.component {...props} />
                          )} />
                      ) : (null);
                    })}
                    <Redirect from="/" to="/dashboard" />
                  </Switch>
                </Suspense>
              </Container>
            </main>
            {/* <AppAside fixed>
              <Suspense fallback={this.loading()}>
                <DefaultAside />
              </Suspense>
            </AppAside> */}
          </div>
          <AppFooter>
            <Suspense fallback={this.loading()}>
              <DefaultFooter />
            </Suspense>
          </AppFooter>
        </div>
      );
  }
  else{
    return(<h2>loading ...</h2>);
   
  }
  }
}

export default DefaultLayout;
